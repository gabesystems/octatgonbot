const Discord = require("discord.js");
const client = new Discord.Client();
const config = require("./config.json");
const ytdl = require("ytdl-core");
const ytsearch = require('youtube-search');

var tempramstor = {}

const streamOptions = { seek: 0, volume: 0.5, filter: "audioonly" };

var ytopts = {
  maxResults: 1,
  key: 'AIzaSyBP4mvJax6NlMYWuvZJI0A195BgZaTtW5A'
};

function playmusic(connection, message) {
  const queue = tempramstor[message.guild.id];
  queue.dispatcher = connection.playStream(ytdl(tempramstor[message.guild.id].queue[0], streamOptions));
  queue.now = tempramstor[message.guild.id].queue[0]
  queue.queue.shift();
  queue.name.shift();
  queue.dispatcher.on("end", function() {
    if (queue.queue[0]) playmusic(connection, message);
    else {
      connection.disconnect();
      queue.dispatcher = null
    }
  })
}

var servers = {};

client.on("ready", () => {
  console.log(`Bot has started, with ${client.users.size} users, in ${client.channels.size} channels of ${client.guilds.size} guilds.`); 
  client.user.setActivity("Octagon | o/help | Bot created by: [CrypticNode.host] Gabe#7116");
});

client.on("guildCreate", guild => {
  console.log(`New guild joined: ${guild.name} (id: ${guild.id}). This guild has ${guild.memberCount} members!`);
  client.user.setActivity("Octagon | o/help | Bot created by: [CrypticNode.host] Gabe#7116");
});

client.on("guildDelete", guild => {
  console.log(`I have been removed from: ${guild.name} (id: ${guild.id})`);
  client.user.setActivity("Octagon | o/help | Bot created by: [CrypticNode.host] Gabe#7116");
});


client.on("message", async message => {
  if(message.author.bot) return;
  if(message.content.indexOf(config.prefix) !== 0) return;

  const args = message.content.slice(config.prefix.length).trim().split(/ +/g);
  const command = args.shift().toLowerCase();
    
  if(command === "ping") {
    const m = await message.channel.send("Ping?");
    m.edit(`Pong! Latency is ${m.createdTimestamp - message.createdTimestamp}ms. API Latency is ${Math.round(client.ping)}ms`);
  }
  
  if(command === "say") {
    const sayMessage = args.join(" ");
    message.delete().catch(O_o=>{}); 
    message.channel.send(sayMessage);
  }
  
  if(command === "kick") {
    if(!message.member.hasPermission("MANAGE_MESSAGES")) return message.channel.send("Insufficient Permissions.");
    if(args[0] == "help"){
      message.reply("Usage: o/kick <user> <reason>");
      return;
    }
    let kUser = message.guild.member(message.mentions.users.first() || message.guild.members.get(args[0]));
    if(!kUser) return message.channel.send("Can't find user!");
    let kReason = args.join(" ").slice(22);
    if(kUser.hasPermission("MANAGE_MESSAGES")) return message.channel.send("That person can't be kicked!");

    let kickEmbed = new Discord.RichEmbed()
    .setDescription("KICK LOG")
    .setColor("#ff5900")
    .addField("Kicked User", `${kUser} with ID ${kUser.id}`)
    .addField("Kicked By", `<@${message.author.id}> with ID ${message.author.id}`)
    .addField("Kicked In", message.channel)
    .addField("Kicked On", message.createdAt)
    .addField("Reason", kReason);

    let logsChannel = message.guild.channels.find(`name`, "logs");
    if(!logsChannel) return message.channel.send("Logs channel not found. Please create a channel called logs.")

    message.guild.member(kUser).kick(kReason);
    logsChannel.send(kickEmbed);


  }
  
  if(command === "ban") {
    if(!message.member.hasPermission("MANAGE_MEMBERS")) return message.channel.send("Insufficient Permissions.");
    if(args[0] == "help"){
      message.reply("Usage: o/ban <user> <reason>");
      return;
    }
    let bUser = message.guild.member(message.mentions.users.first() || message.guild.members.get(args[0]));
    if(!bUser) return message.channel.send("Can't find user!");
    let bReason = args.join(" ").slice(22);
    if(bUser.hasPermission("MANAGE_MESSAGES")) return message.channel.send("That person can't be banned!");

    let banEmbed = new Discord.RichEmbed()
    .setDescription("BAN LOGS")
    .setColor("#bc0000")
    .addField("Banned User", `${bUser} with ID ${bUser.id}`)
    .addField("Banned By", `<@${message.author.id}> with ID ${message.author.id}`)
    .addField("Banned In", message.channel)
    .addField("Time", message.createdAt)
    .addField("Reason", bReason);

    let logsChannel = message.guild.channels.find(`name`, "logs");
    if(!logsChannel) return message.channel.send("Logs channel not found. Please create a channel called logs.")

    message.guild.member(bUser).ban(bReason);
    logsChannel.send(banEmbed);
  }
  
  if(command === "clear") {

    const deleteCount = parseInt(args[0], 10);
    
    if(!deleteCount || deleteCount < 2 || deleteCount > 100)
      return message.reply("Please provide a number between 2 and 100 for the number of messages to delete");
    
    const fetched = await message.channel.fetchMessages({limit: deleteCount});
    message.channel.bulkDelete(fetched)
      .catch(error => message.reply(`Couldn't delete messages because of: ${error}`));
  }

  if(command === "report") {
    let rUser = message.guild.member(message.mentions.users.first() || message.guild.members.get(args[0]));
    if(!rUser) return message.channel.send("Couldn't find user.");
    let rreason = args.join(" ").slice(22);

    let reportEmbed = new Discord.RichEmbed()
    .setDescription("REPORT LOG")
    .setColor("#303030")
    .addField("Reported User", `${rUser} with ID: ${rUser.id}`)
    .addField("Reported By", `${message.author} with ID: ${message.author.id}`)
    .addField("Channel", message.channel)
    .addField("Time", message.createdAt)
    .addField("Reason", rreason);
    let logsChannel = message.guild.channels.find(`name`, "logs");
    if(!logsChannel) return message.channel.send("Logs channel not found. Please create a channel called logs.")
    message.delete().catch(O_o=>{});
    logsChannel.send(reportEmbed);
    return;
  }
  if (command === "play") {
    args.shift();
    var words = args.join(" ");
    console.log("Searching for video: " + words);
    var searchytdl;
    ytsearch(words, ytopts, function(err, searchytdl) {
      if(err) {
        console.log(err);
        message.channel.send("There was an Error.");
      }
      if(!searchytdl[0]) {
        console.log("Failed to find video");
        message.channel.send("Could not find the video you were looking for.");
        return;
      }
      message.member.voiceChannel.join().then(connection => {
      var ytvid = searchytdl[0].link;
      console.log(ytvid);
      if(!tempramstor[message.guild.id]) {tempramstor[message.guild.id] = {queue: [], name: []}}
      tempramstor[message.guild.id].queue.push(ytvid);
      tempramstor[message.guild.id].name.push(searchytdl[0].title);
      if (tempramstor[message.guild.id].dispatcher) {
        console.log("Queued " + ytvid);
        message.channel.send("Queued **" + searchytdl[0].title + "**");
      } else {
      console.log("Playing " + ytvid);
      playmusic(connection, message);
      message.channel.send("Playing **" + searchytdl[0].title + "**");
      }
      });
    });
  }

  if (command === "skip") {
    if (tempramstor[message.guild.id].dispatcher) {
      tempramstor[message.guild.id].dispatcher.end();
      message.channel.send("Skipped!");
    } else {
      message.channel.send("Nothing is being played here.");
    }
  }

  if (command === "leave") {
    message.member.voiceChannel.leave();
  }

  if(command === "summon") {
    message.member.voiceChannel.join();
  }

  if(command === "help") {
    let uicon = message.author.avatarURL;
    let serverembed = new Discord.RichEmbed()
    .setAuthor("Author Name", uicon)
    .setColor("#303030")
    .setFooter("Requested by, " + message.author.userame + ".")
    .setTimestamp()
    .addField("General", "`o/help` - Brings up the help menu." + "\n" + "`o/fidgetspinner` - Spins a fidget spinner." + "\n" + "`o/fakeban` - Pretends to ban a user." + "\n" + "`o/play` - Plays a song of your choice." + "\n" + "`o/skip` - Skips a song." + "\n" + "`o/leave` - Bot will exit voice channel." + "\n" + "`o/summon` - Bot will join voice channel." + "\n" + "`o/say` - Bot will say what you want." + "\n" + "`o/unflip` - Unflips table." + "\n" + "`o/flip` - Flips table.", true)
    .addField("Administration", "`o/kick` - Kicks a specified user." + "\n" + "`o/ban` - Bans a specified user." + "\n" + "`o/tempmute` - Temporarily mutes a specified user." + "\n" + "`o/report` - Creates a report." + "\n" + "`o/warn` - Warns a user.", true);
    return message.channel.send(serverembed);
  }

  if(command === "unflip"){
    message.channel.send("(╯°□°)╯  ︵  ┻━┻").then(m => {
      setTimeout(() => {
          m.edit("(╯°□°)╯    ]").then(ms => {
              setTimeout(() => {
                  ms.edit("(°-°)\\ ┬─┬")
              }, 500)
          })
      }, 500);

    });
  }

  if(command === "flip") {
    message.channel.send("(°-°)\\ ┬─┬").then(m => {
      setTimeout(() => {
          m.edit("(╯°□°)╯    ]").then(ms => {
              setTimeout(() => {
                  ms.edit("(╯°□°)╯  ︵  ┻━┻")
              }, 500)
          })
      }, 500);

    });
  }
});

client.login(config.token);